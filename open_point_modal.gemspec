
lib = File.expand_path("../lib", __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require "open_point_modal/version"

Gem::Specification.new do |spec|
  spec.name          = "open_point_modal"
  spec.version       = OpenPointModal::VERSION
  spec.authors       = ["Ryan Kingston"]
  spec.email         = ["ryanmk54@gmail.com"]

  spec.summary       = %q{Modal to show open points for any object}
  spec.homepage      = "https://bitbucket.org/standardind/open_point_modal.git"

  # Prevent pushing this gem to RubyGems.org. To allow pushes either set the 'allowed_push_host'
  # to allow pushing to a single host or delete this section to allow pushing to any host.
  if spec.respond_to?(:metadata)
    spec.metadata["allowed_push_host"] = "TODO: Set to 'http://mygemserver.com'"
  else
    raise "RubyGems 2.0 or newer is required to protect against " \
      "public gem pushes."
  end

  spec.files         = `git ls-files -z`.split("\x0").reject do |f|
    f.match(%r{^(test|spec|features)/})
  end
  spec.bindir        = "exe"
  spec.executables   = spec.files.grep(%r{^exe/}) { |f| File.basename(f) }
  spec.require_paths = ["lib"]

  spec.add_dependency "railties", ">= 4.2.0"
  spec.add_dependency "angular-ui-bootstrap-rails", "~>2.4.0"
  spec.add_dependency "angular-rails-templates", "~>1.0"

  spec.add_development_dependency "bundler", "~> 1.16"
  spec.add_development_dependency "rake", "~> 10.0"
end
