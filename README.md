# OpenPointModal

Displays open points for a given has\_open\_points\_type and has\_open\_points\_id

## Installation

Add this line to your application's Gemfile:

```ruby
gem 'open_point_modal', git: 'https://bitbucket.org/standardind/open_point_modal.git'
```

And then execute:

    $ bundle

Or install it yourself as:

    $ gem install open_point_modal


Add this line to application.js
```js
//= require open_point_modal
```
Add this line to application.css
```js
//*= require open_point_modal
```

## Adding an app to the allowed apps list
In the /app/controllers/api/open_points_controller near the bottom, there is code similar to the following:
```ruby
    url_to_has_open_points_type_map = {
      'ar' => ['ArlLease'],
      'ar-lease' => ['ArlLease']
    }
```
The key in the url_to_has_open_points_type_map is the app taken from the url of the token.
For example, if the url in the token was 
http://www.standardbusinessmodel.com/po-tracker/?token=zlQeWLKylyLNoDIhTvcsGHVHwBOJRfNxFesmAAfVDYaQAwdyEj
the key would be po-tracker. The value is a list of has_open_points_type's that this app is allowed to access. 
In the case of the above map, the ar and ar-lease apps are only allowed to access open_points with has_open_points_type 'ArlLease'

## Usage

Add OpenPointModal module as a dependency of your app and a dependency of your controller.
```js
angular.module("my-app", ["OpenPointModal"])

.controller("myController", ["OpenPointModal",
function(OpenPointModal) {
}])
```

Call OpenPointModal as shown below
```js
var options = {
  title: "Open Points", // default
  has_open_points_id: lease.id, // required
  has_open_points_type: "ArlLease", // required
  url: generateUrl(lease), // required
  get_all_open_points: false // default
};
OpenPointModal(options);
```
* title: the title of the modal
* has\_open\_points\_id: the id of the object to attach open points to
* has\_open\_points\_type: the type of the object to attach open points to
* url: a url that will take users to the object
* get\_all\_open\_points: if true, it will get all open points belonging to the object
    otherwise, it will only get open points created by or assigned to the current employee
## Development

After checking out the repo, run `bin/setup` to install dependencies. You can also run `bin/console` for an interactive prompt that will allow you to experiment.

To install this gem onto your local machine, run `bundle exec rake install`. To release a new version, update the version number in `version.rb`, then push up to BitBucket.

## Contributing

Code for this repository is hosted at https://bitbucket.org/standardind/open_point_modal.git Talk to Phil to get access.

This repo uses 2 spaces for indentation.
