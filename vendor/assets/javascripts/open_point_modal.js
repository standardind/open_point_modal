//= require_tree ./templates
angular.module('OpenPointModal', ['ui.bootstrap', 'ngSanitize', 'ngResource', 'templates'])
.factory('OpenPointModal', ['$uibModal', '$filter', function($uibModal, $filter) {

  var me = {};
  function controller($scope, $uibModalInstance, sbmOpResource) {
    $scope.modal_title = me.options.title;
    $scope.open_points_status = "Loading open points";
    $scope.open_points = sbmOpResource.OpenPoint.query({
      has_open_points_type: me.options.has_open_points_type,
      has_open_points_id: me.options.has_open_points_id,
      get_all_open_points: me.options.get_all_open_points
    },
      function() {
        $scope.open_points_loaded = true;
        $scope.open_points_status = 'Loaded open points successfully';
      },
      function(response) {
        $scope.open_points_status = 'Unable to load open points';
      }
    );
    $scope.current_employee = sbmOpResource.Employee.current({}, generateCreatedInfo);
    function generateCreatedInfo(employee) {
      $scope.created_info = 'Created by: ' +
        employee.full_name + ' - ' +
        $filter('date')(new Date(), 'mediumDate');
    }

    $scope.priorities = sbmOpResource.Priority.query();

    $scope.searchAssignedTo = me.options.search_all_employees ?
      sbmOpResource.Employee.searchAllEmployees :
      sbmOpResource.Employee.searchDescendantsAndCurrent;

    $scope.selected = {
    };

    $scope.newOpenPoint = function() {
      var open_point = new sbmOpResource.OpenPoint();
      open_point.created_employee = $scope.current_employee;
      open_point.created_at = new Date();
      open_point.has_open_points_type = me.options.has_open_points_type;
      open_point.has_open_points_id = me.options.has_open_points_id;
      open_point.url = me.options.url;
      $scope.open_points.push(open_point);
      $scope.selected.open_point = open_point;
      $scope.editSelectedOpenPoint();
    }

//     $scope.isOpenPointClosedAndExpired = function(open_point) {
//       if (!open_point.closed_at) {return;}
//       var today = new Date();
//       var three_days_ago = new Date(today.getUTCFullYear(),
//                                          today.getUTCMonth(),
//                                          today.getUTCDate() - 3);
//       return open_point.closed_at < three_days_ago;
//     }

    $scope.editSelectedOpenPoint = function(open_point_form) {
      if (!$scope.canEditSelectedOpenPoint()) {return;}
      $scope.selected.object_being_edited = $scope.selected.open_point;

      if (open_point_form) {
        open_point_form.$setPristine;
        open_point_form.$setUntouched;
      }
    }

    $scope.selectOpenPoint = function(open_point) {
      // Don't select an open point if it is already selected
      if ($scope.selected.open_point == open_point) {return;}

      // If they are editing an existing object,
      // have them cancel or save their existing changes
      if ($scope.selected.object_being_edited) {
        focusCancelSave();
        return;
      }
      $scope.selected.open_point = open_point;
    }

    $scope.addResolutionToSelectedOpenPoint = function() {
      var open_point = $scope.selected.open_point;
      var resolution = new sbmOpResource.Resolution();
      resolution.open_point_id = open_point.id;
      setCreatedAttributes(resolution);
      $scope.selected.object_being_edited = resolution;
      open_point.resolutions.push(resolution);
      scrollToCancelButtonOnNextFrame();
    };

    function focusCancelSave() {
      var cancel_edit_btn = document.getElementById('cancel-edit');
      var save_edit_btn = document.getElementById('save-edit');
      cancel_edit_btn.scrollIntoView();
      [cancel_edit_btn, save_edit_btn].forEach(flashRed);

      function flashRed(btn) {
        btn.classList.remove('need-to-cancel-or-save');
        btn.offsetWidth;
          // trigger a reflow
          // (re-calc stuff so adding the class will re-run the animation)
        btn.classList.add('need-to-cancel-or-save');
      }
    }

    function scrollToCancelButtonOnNextFrame() {
      window.setTimeout(function() {
        var cancel_edit_btn = document.getElementById('cancel-edit');
        cancel_edit_btn.scrollIntoView();
      });
    }

    $scope.cancelSelectedOpenPointEdit = function(open_point_form) {
      $scope.selected.object_being_edited = null;
      var open_point = $scope.selected.open_point;

      /* If this open point has been saved, refresh it from the server
       * else, it is a new one, remove it from the list */
      if (open_point.id) {
        if (open_point_form.$pristine) {return;}
        open_point = sbmOpResource.OpenPoint.get({id: open_point.id});
      }
      else {
        var index = $scope.open_points.findIndex(function(cur_op) {
          return cur_op.id == open_point.id;
        });
        $scope.open_points.splice(index);
      }
    }

    $scope.saveSelectedOpenPoint = function(open_point_form) {
      updateOrSaveObject($scope.selected.open_point)
      .then(stopEditingObjectBeingEdited);
    }

    $scope.closeSelectedOpenPoint = function() {
      if (!$scope.canEditSelectedOpenPoint()) {return;}
      $scope.selected.open_point.closed_employee_id = $scope.current_employee.id;
      $scope.selected.open_point.$close();
    }

    $scope.canEditSelectedOpenPoint = function() {
      var open_point = $scope.selected.open_point;
      return !open_point.closed_at &&
        open_point.created_employee.id == $scope.current_employee.id;
    }

    function stopEditingObjectBeingEdited() {
      $scope.selected.object_being_edited = null;
    }

    $scope.editResolution = function(resolution, resolution_form) {
      $scope.editOpenPointChildObject(resolution, resolution_form);
    }

    $scope.editOpenPointChildObject = function(object, form) {
      if (!canEditOpenPointChildObject(object)) {return;}
      $scope.selected.object_being_edited = object;
      setFormPristine(form);
    }

    $scope.saveResolution = function(resolution) {
      resolution = new sbmOpResource.Resolution(resolution);
      updateOrSaveObject(resolution).then(stopEditingObjectBeingEdited);
    }

    $scope.addExceptionToResolution = function(resolution) {
      var exception = {};
      exception.open_point_id = resolution.open_point_id;
      exception.open_point_resolution_id = resolution.id;
      setCreatedAttributes(exception);
      $scope.selected.object_being_edited = exception;
      resolution.exceptions.push(exception);
    }

    $scope.saveException = function(exception) {
      exception = new sbmOpResource.Exception(exception);
      updateOrSaveObject(exception).then(stopEditingObjectBeingEdited);
    }

    function findById(id) {
      return function(obj_in_arr) {
        return obj_in_arr.id == id
      }
    }

    $scope.cancelEdit = function (object, resource_type, object_array, form) {
      $scope.selected.object_being_edited = null

      /* If this object has an id,
       * it exists on the server, so refresh it
       * else, it is a new one, remove it from the list */
      if (object.id) {
        if (form.$pristine) {return;}
        sbmOpResource[resource_type].get({id: object.id}, function(response) {
          // For some reason, setting the object to the response doesn't work
          var index = object_array.findIndex(findById(object.id));
          object_array[index] = response;
        });
      }
      else {
        var index = object_array.findIndex(findById(object.id));
        object_array.splice(index);
      }
    }

    function canEditOpenPointChildObject(object) {
      return object.created_employee.id == $scope.current_employee.id;
    }

    function setFormPristine(form) {
      form.$setPristine;
      form.$setUntouched;
    }

    function setCreatedAttributes(object) {
      object.created_employee = $scope.current_employee;
      object.created_at = new Date();
    }

    function updateOrSaveObject(object) {
      return object.id ? object.$update() : object.$save();
    }

  }


  function showModal(options) {
    var default_options = {
      title: "Open points",
      has_open_points_id: null,
      has_open_points_type: null,
      url: null,
      search_all_employees: false,
      get_all_open_points: false
    };
    Object.keys(default_options).forEach(function(key) {
      if (!options[key]) { options[key] = default_options[key]; }
      if (options[key] === null) { throw key + " is a required option"; }
    });
    me.options = options;

    var modal_instance = $uibModal.open({
      templateUrl: "open_points.html",
      controller: ['$scope', '$uibModalInstance', 'sbmOpResource', controller],
      backdrop: false,
      windowClass: 'op_modal_movable'
    });
    modal_instance.rendered.then(makeModalMovable);
    return modal_instance;
  }


  function resizeModal() {
    var modal = angular.element('.op_modal_movable .modal-dialog');
    var modal_header = modal.find('.modal-header');
    var modal_body = modal.find('.modal-body');

    var margin_top = Number.parseInt(window.getComputedStyle(modal[0]).marginTop);
    var max_height = window.innerHeight - 2 * margin_top -75 + 'px';
    modal_body.css({
      'max-height': max_height,
      'overflow-y': 'auto'
    });
    modal_body.css('max-height', max_height);
  }


  function makeModalMovable() {
    var modal = angular.element('.op_modal_movable .modal-dialog');
    var modal_header = modal.find('.modal-header');
    var modal_body = modal.find('.modal-body');
    modal.on('mousedown', startDragging);

    var start_x = 0, start_y = 0;
    var margin_top = Number.parseInt(window.getComputedStyle(modal[0]).marginTop);
    var x = modal.position().left;
    var y = modal.position().top + margin_top;

    resizeModal();
    window.addEventListener('resize', resizeModal);

    function startDragging(event) {
      var interactable_element_classes = ['.glyphicon', '.btn'];
      if (interactable_element_classes.some(targetContainsClass)) {return;}

      function targetContainsClass(klass) {
        return event.target.classList.contains(klass);
      }

      modal.css('cursor', 'move');
      start_x = event.screenX - x;
      start_y = event.screenY - y;
      angular.element(document).on('mousemove', moveModal);
      angular.element(document).on('mouseup', stopMovingModal);
    }

    function moveModal(event) {
      x = event.screenX - start_x;
      y = event.screenY - start_y;
      modal.css({
        top: y + 'px',
        left: x + 'px'
      });
    }

    function stopMovingModal(event) {
      modal.css('cursor', '');
      angular.element(document).unbind('mousemove', moveModal);
      angular.element(document).unbind('mouseup', stopMovingModal);
    }
  }
  return showModal;
}])


.factory('sbmOpResource', ['$http', '$resource', function($http, $resource) {
  var OpenPoint = new function() {
    var headers = {Authorization: url_helper.token};
    var resource = $resource('/api/open_points/:id/:action.json', {id: '@id'}, {
      query: {headers: headers, isArray: true,
        transformResponse: transformQueryResponse},
      save: {method: 'POST', headers: headers,
        transformRequest: transformUpdateOp,
        transformResponse: transformUpdateOpResponse},
      update: {method: 'PATCH', headers: headers,
        transformRequest: transformUpdateOp,
        transformResponse: transformUpdateOpResponse},
      close: {method: 'POST', params: {action: 'close'}, headers: headers,
        transformRequest: transformUpdateOp,
        transformResponse: transformUpdateOpResponse}
    });
    return resource;

    function transformQueryResponse(data, headersGetter) {
      var open_points = angular.fromJson(data);
      return open_points.map(convertStrToDate);
    }

    function transformUpdateOp(data, headersGetter) {
      var open_point = {
        created_at: data.created_at,
        created_employee_id: data.created_employee.id,
        description: data.description,
        url: data.url,
        due_at: data.due_at,
        has_open_points_id: data.has_open_points_id,
        has_open_points_type: data.has_open_points_type,
        prepared_employee_id: data.prepared_employee.id,
        priority_id: data.priority.id,
      }
      return angular.toJson(open_point);
    }

    function transformUpdateOpResponse(data, headersGetter) {
      var open_point = angular.fromJson(data);
      return convertStrToDate(open_point);
    }

    function convertStrToDate(open_point) {
      open_point.due_at = new Date(open_point.due_at);
      open_point.created_at = new Date(open_point.created_at);
      if (open_point.closed_at) {
        open_point.closed_at = new Date(open_point.closed_at);
      }
      open_point.resolutions.forEach(convertResolutionStrToDate);
      return open_point;
    }

    function convertResolutionStrToDate(resolution) {
      resolution.created_at = new Date(resolution.created_at);
      resolution.exceptions.forEach(convertExceptionStrToDate);
    }

    function convertExceptionStrToDate(exception) {
      exception.created_at = new Date(exception.created_at);
      if (!exception.resolutions) {return;}
      exception.resolutions.forEach(convertResolutionStrToDate);
    }
  };
  var Resolution = $resource('/sbm/open_point_resolutions/:id.json', {id: "@id"}, {
    update: {method: "PUT"}, cache: true})
  var Exception = $resource('/sbm/open_point_exceptions/:id.json', {id: "@id"}, {
    update: {method: "PUT"}, cache: true})


  var Employee = new function() {
    var resource = $resource('/sbm/employees/:id/:action.json', {id: '@id'}, {
      current: {
        method: 'GET', url: '/sbm/employees/current_employee.json',
        isArray: false, cache: true
      },
      descendants: {
        method: 'GET', url: '/sbm/employees/:id/descendants.json',
        isArray: true, cache: true
      }
    });

    function searchAllEmployees(search_string) {
      return resource.query({search: search_string}).$promise;
    }

    function searchDescendantsAndCurrent(search_string) {
      var current = resource.current();
      return current.$promise.then(searchDescendants).then(addCurrentIfItMatches);

      function searchDescendants() {
        return resource.descendants({
          id: current.id,
          search_string: search_string
        }).$promise;
      }

      function addCurrentIfItMatches(descendants) {
        var full_name = current.full_name.toLowerCase();
        var search_str = search_string.toLowerCase();
        if (full_name.indexOf(search_str) != -1) {
          return descendants.concat(current);
        }
        return descendants;
      }
    }

    return {
      current: resource.current,
      descendants: resource.descendants,
      searchDescendantsAndCurrent: searchDescendantsAndCurrent,
      searchAllEmployees: searchAllEmployees
    };
  };


  var Priority = $resource('/sbm/priorities/:id.json', {id: '@id'});


  return {
    OpenPoint: OpenPoint,
    Employee: Employee,
    Priority: Priority,
    Resolution: Resolution,
    Exception: Exception
  };
}])
